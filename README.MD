# Essay

This essay is written in LaTeX and is automatically compiled in a Continuous Integration pipeline, the latest version of the resulting [PDF is available here](https://personal_essays.gitlab.io/south-china-sea-chinese-claims/main.pdf)
